import axios from "axios";
import store from "../store";

export const fetchMobileStart=()=>{
    return {
        type: "FETCH_MOBILES"
    };
}


export const mobileDetails = data => {
    return (dispatch) => dispatch({
        type: "MOBILE_DETAILS",
        data: data
    });
};

export const receive_error = () => {
    return {
        type: "RECEIVE_ERROR"
    };
};

export const getMobiles = (pageNo=1) => {
   /* console.log("pageNo",pageNo)
    pageNo=pageNo?pageNo:1;*/
    store.dispatch(fetchMobileStart());
    return (dispatch) => axios.get(`http://localhost:5000/phones/?page=1`)
        .then(res => {
            console.log("res",res)
            dispatch({
                type: "FETCHED_MOBILES",
                data: res.data
            });
        }).catch(() => {
            dispatch({
                type: "RECEIVE_FETCHED_MOBILES_ERROR",
                data: ''
            });
        })
};

