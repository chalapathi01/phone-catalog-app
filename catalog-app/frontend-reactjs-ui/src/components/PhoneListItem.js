import React, {Component} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {mobileDetails} from "../actions/fetchAction";

class PhoneListItem extends Component {
    componentDidMount() {
        this.props.mobileDetails(this.props.product)
    }

    onClickHandler = () => {
        this.props.history.push({
            pathname: `/phones/${this.props.product.id}`,
            state: this.props.product
        })
    }


    render() {
        const {product} = this.props;
        return (
            <div className="card" onClick={() => {
                this.onClickHandler()
            }} style={{marginBottom: "10px"}}>
                <div className="card-body">
                    <div className={"row"}>
                        <div className={"col-md-6"}>
                            <h4 className="card-title">{product.brand}</h4>
                            <h6 className="card-title">{product.model}</h6>
                            <h5 className="card-text"><small>color: </small>{product.color}</h5>
                            <h5 className="card-text"><small>price: </small>${product.price}</h5>
                            <p className="card-text">{product.description}</p>
                        </div>
                        <div className={"col-md-6"}>
                            {product.images.length > 0 && product.images.map((image, i) => {
                                return <img src={image} key={image} alt={i} width={50} height={50}/>
                            })}                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        mobileDetails: (data) => {
            dispatch(mobileDetails(data));
        }
    };
};
const mapStateToProps = state => {
    return {
        data: state
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PhoneListItem));