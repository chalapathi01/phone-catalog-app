import React, {Component} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class PhoneDetailComponent extends Component {


    render() {
        const product = this.props.location.state;

        return (
            <div className="container">
                <Link to={"/"}> <span dangerouslySetInnerHTML={{__html:'&#8592;'}}/> back</Link>
                <div className="card">
                    <div className={"img-detail-container"}>
                        {product.images.map((image,i) =>
                            <img src={image} alt='img' width={200} height={200} key={i}/>)}
                    </div>
                    <div className="card-body">
                        <h4 className="card-title">{product.brand}</h4>
                        <h6 className="card-title">{product.model}</h6>
                        <h5 className="card-text"><small>color: </small>{product.color}</h5>
                        <h5 className="card-text"><small>price: </small>${product.price}</h5>
                        <p className="card-text">{product.description}</p>

                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        mobilesListData: state.mobilesListData
    };
};
export default connect(mapStateToProps)(PhoneDetailComponent);