import React from "react";
import {getMobiles} from "../actions/fetchAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

const Pagination = props => {
    let page = props.match.params.page;
    page = page ? parseInt(page) : 1;
    return (
        <div className="page-container">
            <Link to={`/phones/page/${page > 1 ? page - 1 : 1}`}>
                prev
            </Link>
            <div>
                Page no : {page}
            </div>
            <Link to={`/phones/page/${page ? page + 1 : 1}`}>
                next
            </Link>
        </div>
    );
};
const mapDispatchToProps = dispatch => {
    return {
        getMobiles: (page) => {
            dispatch(getMobiles(page));
        }
    };
};

const mapStateToProps = state => {
    return {
        mobilesListData: state.mobilesListData,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);

