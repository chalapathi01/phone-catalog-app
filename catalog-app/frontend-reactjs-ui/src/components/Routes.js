import React from "react";
import {Route, Switch, Redirect} from 'react-router-dom';
import PhoneListContainer from "./PhoneListContainer";
import PhoneDetailComponent from "./PhoneDetailComponent";

class Routes extends React.Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" render={() => <Redirect to={"/phones"}/>}/>
                    <Route exact path="/phones" component={PhoneListContainer} />
                    <Route exact path="/phones/page/:page" component={PhoneListContainer} />

                    <Route exact path="/phones/:id" component={PhoneDetailComponent}/>
                    <Route default component={Error}/>
                </Switch>
            </div>
        )
    }


}

export default Routes;