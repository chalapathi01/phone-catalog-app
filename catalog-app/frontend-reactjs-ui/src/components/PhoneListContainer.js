import React, {Component} from "react";
import {connect} from "react-redux";
import {getMobiles} from "../actions/fetchAction";
import Pagination from "./Pagination";
import PhoneListItem from "./PhoneListItem";
import MoonLoader from "react-spinners/ClipLoader";


class PhoneListContainer extends Component {
    constructor(props) {
        super(props);
        this.pageNo = undefined;
    }

    componentDidMount() {
        let page = this.props.match.params.page;
        this.pageNo = page ? parseInt(page) : 1;
        console.log("page",page)
        this.props.getMobiles(this.pageNo);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        let page = parseInt(nextProps.match.params.page);
        if (page&&page !== this.pageNo) {
            this.pageNo = page;
            this.props.getMobiles(page);
        }

    }

    render() {
        const {mobilesList, mobilesListData, isLoading} = this.props;
        return (<>{
            isLoading ? <div className={"loader-container"}><MoonLoader size={60} radius={2}/></div> :
                <div className="container">
                    <h3 className="card-title">List of Available Products</h3>
                    <hr/>
                    {mobilesList && mobilesList.map(mobile => {
                        return <PhoneListItem product={mobile} key={mobile.id}/>
                    })}
                    {mobilesListData && mobilesListData.page > 0 &&
                    <Pagination pageNo={mobilesListData.page}  {...this.props}/>}
                </div>}
        </>);
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getMobiles: (page) => {
            dispatch(getMobiles(page));
        }
    };
};

const mapStateToProps = state => {
    return {
        mobilesList: state.mobilesList,
        mobilesListData: state.mobilesListData,
        isLoading: state.isLoading
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneListContainer);