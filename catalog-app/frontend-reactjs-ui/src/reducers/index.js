const initialState = {
    mobilesListData: {},
    isLoading:false,
    mobilesList: [],
    productDetails: ''

};

const asyncReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCHED_MOBILES":
            return Object.assign({}, state, {
                mobilesListData: action.data,
                mobilesList: action.data.docs,
                isLoading:false
            });

        case "FETCH_MOBILES":
            return Object.assign({}, state, {
                isLoading:true
            });

        case "MOBILE_DETAILS":
            return Object.assign({}, state, {
                productDetails: action.data,
                isLoading:false
            });
        default:
            return state;
    }
};

export default asyncReducer;