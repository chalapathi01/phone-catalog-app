import React, { Component } from "react";
import { connect } from "react-redux";
import Routes from "./components/Routes";
import './App.css';

class App extends Component {

  render() {
    return (<Routes/>);
  }
}

const mapStateToProps = state => {
  return {
    data: state
  };
};
export default connect(mapStateToProps)(App);