This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


### /catalog-app/back-end/`npm install`

### /catalog-app/back-end/`npm start`

Runs the app in the development mode.<br />
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### /catalog-app/frontend-reactjs-ui/`npm install`

## Available Scripts

In the project directory, you can run:

### /catalog-app/frontend-reactjs-ui/`npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`
